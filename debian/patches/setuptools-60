From: Stefano Rivera <stefanor@debian.org>
Date: Sat, 12 Nov 2022 15:14:59 +0200
Subject: Import setuptools before distutils

setuptools 60 uses its own bundled version of distutils, by default. It
injects this into sys.modules, at import time. So we need to make sure
that it is imported, before anything else imports distutils, to ensure
everything is using the same distutils version.

This change in setuptools is to prepare for Python 3.12, which will drop
distutils.

Forwarded: https://github.com/benoitc/http-parser/pull/97
Bug-Debian: https://bugs.debian.org/1022468
---
 setup.py | 8 ++++----
 1 file changed, 4 insertions(+), 4 deletions(-)

diff --git a/setup.py b/setup.py
index 5c3f768..5cdd5e1 100644
--- a/setup.py
+++ b/setup.py
@@ -4,10 +4,6 @@
 # See the NOTICE for more information.
 
 from __future__ import with_statement
-from distutils.errors import CCompilerError, DistutilsExecError, \
-    DistutilsPlatformError
-from distutils.command.build_ext import build_ext
-from distutils.command.sdist import sdist as _sdist
 import glob
 from imp import load_source
 import io
@@ -17,6 +13,10 @@ import sys
 import traceback
 
 from setuptools import setup, find_packages, Extension
+from distutils.errors import CCompilerError, DistutilsExecError, \
+    DistutilsPlatformError
+from distutils.command.build_ext import build_ext
+from distutils.command.sdist import sdist as _sdist
 
 if not hasattr(sys, 'version_info') or \
         sys.version_info < (2, 6, 0, 'final'):
